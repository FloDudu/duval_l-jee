package fr.epita.iam.web.actions;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.Identity;
import fr.epita.iamcore.services.dao.IdentityDAO;

public abstract class AbstractServlet extends HttpServlet {
	private static final long serialVersionUID = 3564906256288907293L;
	private static IamLogger LOGGER = IamLogManager.getIamLogger(AbstractServlet.class);
	
	@Inject
	protected IdentityDAO dao;
	
	protected Identity getIdentity(String parameter) throws ParameterException {
		if (parameter == null)
			throw new ParameterException("Parameter is null");
		if (parameter.equals(""))
			throw new ParameterException("Parameter is empty");
		try {
			Long id = new Long(parameter);
			Identity identity = dao.getById(id);
			if (identity == null)
				throw new ParameterException("Parameter does not correspond to any Identity");
			return identity;
		} catch (Exception e) {
			throw new ParameterException(e.getMessage());
		}
	}
}
