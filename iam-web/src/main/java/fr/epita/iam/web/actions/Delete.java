package fr.epita.iam.web.actions;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.Identity;
import fr.epita.iamcore.services.dao.IdentityDAO;

public class Delete extends HttpServlet {
	private static final long serialVersionUID = -6664301205760084808L;
	private static IamLogger LOGGER = IamLogManager.getIamLogger(Delete.class);

	@Inject
	private IdentityDAO dao;
	
	/**
	 * @see HttpServlet#doDelete(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Long id = new Long(req.getParameter("identity"));
		Identity toDelete = dao.getById(id);
		if (toDelete != null)
			LOGGER.info(toDelete);
	}
}
