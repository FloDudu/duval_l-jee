package fr.epita.iam.web.actions;

public class ParameterException extends Exception {
	private static final long serialVersionUID = -6238342275841320818L;

	public ParameterException(String msg) {
		super(msg);
	}
	
	public ParameterException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
