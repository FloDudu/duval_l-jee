package fr.epita.iam.webservices;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import fr.epita.iamcore.services.dao.IdentityDAO;

@WebService
public class IdentityWebServiceImpl implements IdentityWebservice {

	@Inject
	private IdentityDAO dao;
	
	@WebMethod
	@Override
	public int countIdentities() {
		return dao.getCount();
	}

}
