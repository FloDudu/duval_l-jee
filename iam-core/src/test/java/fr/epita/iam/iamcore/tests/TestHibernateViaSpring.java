/**
 * 
 */
package fr.epita.iam.iamcore.tests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.Identity;
import fr.epita.iamcore.datamodel.User;
import fr.epita.iamcore.services.dao.IdentityDAO;
import fr.epita.iamcore.services.dao.UserDAO;

/**
 * @author tbrou
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml"})
public class TestHibernateViaSpring {
	
	//hack pour fermer la session factory
	private static SessionFactory globalsf;
	
	
	@Inject
	DataSource ds;
	
	@Inject
	SessionFactory sf;
	
	@Inject
	private IdentityDAO dao;
	
	@Inject
	private UserDAO userDAO;
	
	private static final IamLogger LOGGER = IamLogManager.getIamLogger(TestHibernateViaSpring.class);
	
	
	@Before
	public void setUp(){
		if (globalsf == null){
			globalsf = sf;
		}
	}
	
	@Test
	public void testDataSource() throws SQLException{
		Assert.assertNotNull(ds);
		LOGGER.info(ds.getConnection().getSchema());
	}
	
	@Test
	public void testSaveUser()  throws SQLException {
		// Given
		User user = new User("user", "password");
		
		// When
		userDAO.save(user);
		Connection connection = ds.getConnection();
		PreparedStatement st = connection.prepareStatement("select * from USERS where username = ?");
		st.setString(1, "user");
		ResultSet rs = st.executeQuery();
		Assert.assertEquals(1, rs.getFetchSize());
		rs.next();
		String fetchUsername = rs.getString("username");
		String fetchPassword = rs.getString("password");
		Assert.assertEquals("user", fetchUsername);
		Assert.assertEquals("password", fetchPassword);
		LOGGER.info("fetched user : {} - {}", fetchUsername, fetchPassword);
		
		st.close();
		connection.close();
		User test = userDAO.getById(Long.valueOf(1));
		Assert.assertNotNull(test);
		LOGGER.info(test);
	}

	
	@AfterClass
	public static void tearDownGlobal(){
		globalsf.close();
	}
}
