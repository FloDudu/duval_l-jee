package fr.epita.iamcore.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USERS")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String username;
	private String password;
	
	/**
	 * User's default constructor
	 */
	public User() {}
	
	/**
	 * User's full constructor
	 * @param username The username
	 * @param password The password
	 */
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	
	public final Long getId() {
		return id;
	}
	
	/**
	 * User username getter
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Set the user's username
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * User password getter
	 * @return the password
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * Set the user's password
	 * @param password
	 */
	public final void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return String.format("User [id=%d, username=%s, password=********]", id, username);
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		// Check ID
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		// Check USERNAME
		if (username == null) {
			if (other.getUsername() != null)
				return false;
		} else if (!username.equals(other.getUsername()))
			return false;
		// Check PASSWORD
		if (password == null) {
			if (other.getPassword() != null)
				return false;
		} else if (!password.equals(other.getPassword()))
			return false;

		return true;
	}
}
