package fr.epita.iamcore.datamodel;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="IDENTITIES")
public class Identity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String fname;
	private String lname;
	private String email;
	private Date birthday;	

	/**
	 * Identity's default constructor
	 */
	public Identity() {}
	
	/**
	 * Identity's full constructor
	 * @param fname The first name
	 * @param lname The last name
	 * @param email The email
	 * @param birthday The birthday date
	 */
	public Identity(String fname, String lname, String email, Date birthday) {
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.birthday = birthday;
	}

	public final Long getId() {
		return id;
	}
	
	/**
	 * @return the displayName
	 */
	public final String getFname() {
		return fname;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public final void setFname(String fname) {
		this.fname = fname;
	}

	public final String getLname() {
		return lname;
	}

	public final void setLname(String lname) {
		this.lname = lname;
	}

	public final String getEmail() {
		return email;
	}

	public final void setEmail(String email) {
		this.email = email;
	}

	public final Date getBirthday() {
		return birthday;
	}
	
	public final String getFormBirthday() {
		String date = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = sdf.format(birthday);
		} catch (Exception e) {
		}
		return date;
	}

	public final void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	@Override
	public final String toString() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String date = "";
		if (birthday != null)
			date = format.format(birthday);
		return String.format("Identity [id=%d, first name=%s, last name=%s, email=%s, birthday=%s]", id, fname, lname, email, date);
	}
}
