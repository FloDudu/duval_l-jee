package fr.epita.iamcore.services.dao.impl;

import java.util.Collection;

import javax.inject.Inject;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import fr.epita.iamcore.datamodel.User;
import fr.epita.iamcore.services.dao.UserDAO;

public class UserJPADAO implements UserDAO {

	@Inject
	private SessionFactory sf;
	
	/**
	 * Get the session factory
	 * @return SessionFactory sf
	 */
	public final SessionFactory getSf() {
		return sf;
	}

	/**
	 * Set the session factory
	 * @param sf
	 */
	public final void setSf(SessionFactory sf) {
		this.sf = sf;
	}

	@Override
	public void save(User id) {
		Session session = sf.openSession();
		session.saveOrUpdate(id);
		session.flush();
		session.close();

	}

	@Override
	public void update(User id) {
		Session session = sf.openSession();
		session.saveOrUpdate(id);
		session.flush();
		session.close();
	}

	@Override
	public void delete(User id) {
		Session session = sf.openSession();
		session.delete(id);
		session.flush();
		session.close();
	}

	@Override
	public User getById(Object id) {
		Session session = sf.openSession();
		Criteria crits = session.createCriteria(User.class)
				.add(Restrictions.eq("id", id));
		return (User) crits.uniqueResult();
	}

	@Override
	public Collection<User> search(Collection<Criterion> restrictions) {
		Session session = sf.openSession();
		Criteria crits = session.createCriteria(User.class);
		for (Criterion r : restrictions) {
			crits.add(r);
		}
		return crits.list();
	}
	
	@Override
	public int getCount() {
		return 0;
	}
}
