package fr.epita.iamcore.services.dao;

import java.util.Collection;
import org.hibernate.criterion.Criterion;

public interface DAO<T> {	
	void save(T id);
	void update(T id); 
	void delete(T id);
	T getById(Object id);
	Collection<T> search(Collection<Criterion> restrictions);
	int getCount();
}
