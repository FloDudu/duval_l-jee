package fr.epita.iamcore.services.dao;

import fr.epita.iamcore.datamodel.User;

public interface UserDAO extends DAO<User> {

}
